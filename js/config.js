define([],function()
{
    var config = {
                    app : {
                                name : 'thomson-reuters'
                          },

                    endpoint : {
                                    base :'db',
                                    clients : 'clients.json'
                               },

                    js :{
                            controller : 'js/controller',
                            view : 'js/view'
                        },

                    /* ANGULAR SPECIFIC CONFIG */
                    ng :  {
                                basePath : 'ng',
                                defaultRoute : {controller : 'client',action : 'index'},
                                route : [
                                            'route/main'
                                        ],
                                suffixes  : ['Repository','Service']
                          },
                    events : {
                                'sortChange' : 'sort-change',
                                routeNotFound : 'route-not-found'
                             },

                    intelligence : {
                                        clientId : 'Thomson_Reuters_1467270132',
                                        clientSecret : '4RCMW843dBJyAvCoFnOrg4zC',
                                        projectId : 30039,
                                        applicationId : 4159
                                   },
                    analyticsEvent : {
                                            view : 'Bambu.TR.view'
                                     }
                 };

    return config;
});