define(['config','helper'],function(config,helper)
{
      return [
                {
                        action : 'index',
                        dependencies : {
                                          url : '/list',
                                          directive:['portfolioValue'],
                                          clients : function(clientRepository)
                                          {
                                              return clientRepository.find()
                                                                     .then(function(list)
                                                                     {
                                                                        return helper.sortBy(list,{'impact.positive':-1,'impact.negative':-1});
                                                                     });
                                          },
                                          labels : function(settingRepository)
                                          {
                                              return settingRepository.getGlobalLabels()
                                                                    .then(function(globalLabels)
                                                                    {
                                                                        return settingRepository.getClientListLabels()
                                                                                                .then(function(labels)
                                                                                                {
                                                                                                    return helper.extend(labels,globalLabels);
                                                                                                });
                                                                    });
                                          }
                                       },
                        isDefault : true,
                        metaData : {
                                       selectedMenu : 'client-list',
                                       analyticsEventName : 'Bambu.TR.View.ClientList'
                                   }
                  },
                  {
                        action : 'info',
                        dependencies : {
                                          url : '/info/{clientId}',
                                          directive : ['portfolioValue','address','assessmentTypeIcon','clientAge','clientAssessmentTypeText','reccomendationAction','recommendationNewsList'],
                                          client : function($stateParams, clientRepository)
                                          {
                                              return clientRepository.findById($stateParams.clientId);
                                          },
                                          reccomendations :function($stateParams,clientRepository)
                                          {
                                              return clientRepository.getRecommendations(parseInt($stateParams.clientId));
                                          }
                                       },
                        metaData : {
                                      selectedMenu : 'client-list',
                                      analyticsEventName : 'Bambu.TR.View.ClientDetail'
                                   }
                  }
             ];
});