define(['config','flow','helper'],function(config,flow,helper){
    return [
                {
                    name : 'app',
                    controller : 'main',
                    dependencies : {
                                      url : '/app',                                      
                                      directive : ['timeTicking','a'],
                                      globalLabels : function(routeService,navigationService,settingRepository,notificationService)
                                      {                                                                            
                                          return settingRepository.getGlobalLabels();
                                      },
                                      urls : function(routeService)
                                      {                                            
                                          return flow.waterfall(
                                                                  {
                                                                      loadRoute : function()
                                                                      {
                                                                          //LOAD THE ROUTE SO WE COULD GENERATE URL
                                                                          return routeService.loads(['client','market']);
                                                                      },
                                                                      urls : function(response)
                                                                      {
                                                                          return {
                                                                                    clientList : routeService.url({controller : 'client'}),
                                                                                    marketList : routeService.url({controller : 'market'})
                                                                                 };
                                                                      }
                                                                  }
                                                               )
                                                      .then(function(response)
                                                      {
                                                          return response.urls;
                                                      });                                         
                                      }
                                   },
                    isSystem : true,
                    metaData : {
                                  
                               }
                }
           ];
});