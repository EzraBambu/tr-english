define(['jquery','helper','text!view/shared/client/recommended-news/summary.html'],function($,helper,template)
{
    return {               
                func : function($compile)
                {
                     return {
                                restrict: 'E',
                                require: 'ngModel',
                                transclude: true,
                                replace: false,
                                template : template,
                                scope : {
                                            ngModel: '=?',
                                        },
                                link: function($scope, $element, attrs,ngModel) 
                                {
                                    $scope.data = {
                                                        news : $scope.ngModel
                                                  };
                                    $scope.handler = {};
                                    $scope.handler.abs = function(value)
                                    {
                                        return Math.abs(value);
                                    }

                                }
                            };
                }
           };
});