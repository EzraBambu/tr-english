define(['helper','moment'],function(helper,moment)
{
    return  function($timeout)
    {
        var $_element = undefined;
        function _refreshTime($element)
        {
            var date = moment(new Date());
            var time = date.format('HH:mm:ss');
            var datePart = date.format('dddd D MMMM / YYYY').toUpperCase();
            $_element.html(time + ' (SGT +08:00) &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; ' + datePart);
            $timeout(_refreshTime,1000);
        }

        return {
                    restrict: 'A',
                    scope : false,
                    link: function($scope, element) 
                    {
                        $_element = element;
                        _refreshTime();
                    }
                };
    }
    

    /*return {              
                func : function($timeout)
                {
                    var $_element = undefined;
                    function _refreshTime($element)
                    {
                        var date = moment(new Date());
                        var time = date.format('HH:mm:ss');
                        var datePart = date.format('dddd D MMMM / YYYY').toUpperCase();
                        $_element.html(time + ' (SGT +08:00) &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; ' + datePart);
                        $timeout(_refreshTime,1000);
                    }

                     return {
                                restrict: 'A',
                                scope : false,
                                link: function($scope, element) 
                                {
                                    $_element = element;
                                    _refreshTime();
                                }
                            };
                }
           };*/
});