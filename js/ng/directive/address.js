define(['helper'],function(helper)
{
    return function($filter)
    {
        return {
                    restrict: 'A',
                    scope : false,
                    link: function($scope, element, attrs) 
                    {
                        var address = attrs['address'].split("\n");
                        var html = helper.map(
                                                address,
                                                function(item)
                                                {
                                                    return "<p>" + item + "</p>";
                                                }
                                             )
                                         .join('');
                        element.replaceWith(html);
                    }
                };
    };
    /*return {
                type : 'directive',
                name : 'address',
                func : function($filter)
                {
                    return {
                                restrict: 'A',
                                scope : false,
                                link: function($scope, element, attrs) 
                                {
                                    var address = attrs['address'].split("\n");
                                    var html = helper.map(
                                                            address,
                                                            function(item)
                                                            {
                                                                return "<p>" + item + "</p>";
                                                            }
                                                         )
                                                     .join('');
                                    element.replaceWith(html);
                                }
                            };
                }
            }*/
})