define(['config','jquery','growl'],function(config,$)
{
    return function($rootScope)
    {
        var self = {
                        info : info,
                        error : error,
                        warning : warning
                   };


        function info(message)
        {
            $.bootstrapGrowl(
                                  message, 
                                  {
                                      ele: 'body', // which element to append to
                                      type: 'info', // (null, 'info', 'danger', 'success')
                                      offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                                      align: 'right', // ('left', 'right', or 'center')
                                      delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                                      allow_dismiss: true, // If true then will display a cross to close the popup.
                                      stackup_spacing: 10 // spacing between consecutively stacked growls.
                                  });
        }

        function error(message)
        {            
             $.bootstrapGrowl(
                                  message, 
                                  {
                                      ele: 'body', // which element to append to
                                      type: 'danger', // (null, 'info', 'danger', 'success')
                                      offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                                      align: 'right', // ('left', 'right', or 'center')
                                      delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                                      allow_dismiss: true, // If true then will display a cross to close the popup.
                                      stackup_spacing: 10 // spacing between consecutively stacked growls.
                                  });
        }

        function warning(message)
        {
            $.bootstrapGrowl(
                              message, 
                              {
                                  ele: 'body', // which element to append to
                                  type: 'danger', // (null, 'info', 'danger', 'success')
                                  offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                                  align: 'right', // ('left', 'right', or 'center')
                                  delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                                  allow_dismiss: true, // If true then will display a cross to close the popup.
                                  stackup_spacing: 10 // spacing between consecutively stacked growls.
                              });
        }

        $rootScope.$on(
                            config.events.routeNotFound,
                            function(e,payload)
                            {
                                self.warning('Route not found.');
                            }
                      );

        return self;
    }
});