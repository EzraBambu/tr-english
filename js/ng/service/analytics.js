define(['config','helper','promise'],function(config,helper,promise) {
    return function($location,notificationService)
    {   
        var _sdk;        
        var _endPoint;
    
        function logRoute(route)
        {
            var url = $location.url();
            var payload = {
                                controller : route.controller,
                                action : route.action,
                                parseUrl : url
                          };
            if(route.dependencies)
            {
                helper.each(
                                helper.keys(route.dependencies),
                                function(key)
                                {
                                    var value = route.dependencies[key];                                    
                                    if(helper.isFunction(value))
                                    {
                                        payload[key] = 'func'; 
                                    }
                                    else if(key === 'directive')
                                    {
                                        payload[key] = value.join(',');
                                    }
                                    else
                                    {
                                        payload[key] = value;
                                    }                                    
                                }
                            );
            }
            var eventName = route.metaData.analyticsEventName || config.analyticsEvent.view;
           /* _sendEvents(eventName,payload);
 */       }

        function _sendEvents(eventName,payload)
        {
            _getEndPoint().then(function(endpoint)
                                {
                                    endpoint.now(eventName,payload);
                                })
                          .fail(function(e)
                               {
                                    _sdk = undefined; //RESET SDK SO IT WILL RE-ESTABLISH CONNECTION
                                    notificationService.error(e);
                               });
        }

        function _getEndPoint()
        {
            if(_sdk === undefined)
            {
                return promise.defered()
                              .then(function(defered)
                              {
                                    /*var sdk = new Intelligence(config.intelligence);
                                    sdk.authenticate(function(e,token)
                                    {
                                        if(e)
                                        {
                                            notificationService.error(e);
                                            defered.reject(e);
                                        }
                                        else
                                        {
                                            _sdk = sdk;
                                            _endPoint = token;
                                            defered.resolve(_endPoint);
                                        }
                                    });*/
 defered.resolve();                                   return defered.promise;
                                });
            }
            else
            {
                return promise.pass(_endPoint);
            }
        }

        return {
                    logRoute : logRoute
               };
    };
});