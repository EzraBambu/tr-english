define([
          'config',
          'q',
          'promise',
          'underscore',
          'helper',
          'angularAMD',
          'angularRouter',
          'angularAnimate',
          'angularSanitize',
          'angularBootstrap'
       ],function(
                    config,
                    q,
                    promise,
                    _,
                    helper,
                    util,
                    routeResolver
                )
{  
    var app = angular.module(
                                config.app.name,
                                [
                                    'ui.router',
                                    'ngAnimate',
                                    'ngSanitize',
                                    'ui.bootstrap'
                                ]
                            )
                      .config([
                                  '$stateProvider', '$urlRouterProvider','$controllerProvider','$compileProvider','$filterProvider','$provide',
                                  init
                              ])
                     .run([
                            '$state',
                            '$injector',
                            run
                          ]
                         );


    function init($stateProvider, $urlRouterProvider,$controllerProvider,$compileProvider,$filterProvider,$provide)
    {
        //THIS WILL SAVE THE REFERENCE THAT WILL BE USE LATER WHEN WE CREATING A DYNAMIC CONTROLLER/SERVICE
        app.register = {
                          controller: $controllerProvider.register,
                          directive: $compileProvider.directive,
                          filter: $filterProvider.register,
                          factory: $provide.factory,
                          service: $provide.service
                      };
        app._globals = {
                          $stateProvider : $stateProvider
                       };

        //LOAD THE ROUTE
        helper.loadRoute($stateProvider);
        var fn = function($injector,$location)
        {
            helper.loadRouteAndGo($stateProvider,$injector,$location.$$url);
        };
        fn.$inject = ['$injector','$location'];
        $urlRouterProvider.otherwise(fn);
    };


    function run($state,$injector)
    { 
        app._globals.$injector = $injector;
    };
   
    return app;
});