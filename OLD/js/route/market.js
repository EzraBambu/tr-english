define(['config','helper','flow'],function(config,helper,flow){
      return [
                {
                        action : 'index',
                        dependencies : {
                                          url : '/list',
                                          recommendations : function(recommendationRepository)
                                          {
                                              return recommendationRepository.getMarketInsight()
                                                                             .then(function(list)
                                                                             {

                                                                                var list = helper.map(list,function(item)
                                                                                                 {
                                                                                                    if(item.marketOrder === undefined)
                                                                                                    {
                                                                                                        item.marketOrder = 0;   
                                                                                                    }
                                                                                                    return item;
                                                                                                 })
                                                                                 return helper.sortBy(list,{marketOrder : -1});
                                                                             });
                                          }                                         
                                       },
                        metaData : {
                                       selectedMenu : 'market-insight',
                                       analyticsEventName : 'Bambu.TR.View.MarketList'
                                   }
                  },
                  {
                        action : 'detail',
                        dependencies : {
                                          url : '/info/{recommendationId}',
                                          directive : ['clientSummary'],
                                          selectedReccomendationId : function($stateParams)
                                          {
                                              return parseInt($stateParams.recommendationId);
                                          },
                                          info : function($stateParams,recommendationRepository,newsRepository)
                                          {
                                              //PROCESS AS FLOW PARALLEL TO AVOID REQUERY FOR THE RECOMMENDATION OBJECT
                                              return recommendationRepository.findById(parseInt($stateParams.recommendationId))
                                                                             .then(function(recommendation)
                                                                             {                                                                                  
                                                                                  return flow.parallel(
                                                                                                          {
                                                                                                              news : function()
                                                                                                              {
                                                                                                                  return newsRepository.getNews(recommendation.newsList);
                                                                                                              },
                                                                                                              clients : function()
                                                                                                              {
                                                                                                                  return recommendationRepository.getClients(recommendation);
                                                                                                              }
                                                                                                          }
                                                                                                      );
                                                                             });
                                              return {};
                                          },
                                          recommendations : function($stateParams,recommendationRepository,newsRepository)
                                          {
                                               return recommendationRepository.getMarketInsight()
                                                                             .then(function(list)
                                                                             {
                                                                                var list = helper.map(list,function(item)
                                                                                                 {
                                                                                                    if(item.marketOrder === undefined)
                                                                                                    {
                                                                                                        item.marketOrder = 0;   
                                                                                                    }
                                                                                                    return item;
                                                                                                 })
                                                                                 return helper.sortBy(list,{marketOrder : -1})
                                                                                              .then(function(list)
                                                                                              {
                                                                                                    // BY PAOLA REQUEST, IF THE RECOMMEDATION HAS NO LINKS LINK TO IT, REMOVE IT FOR NOW
                                                                                                    return helper.filter(
                                                                                                                            list,
                                                                                                                            function(recommendation)
                                                                                                                            {
                                                                                                                                return helper.isArray(recommendation.newsList) && recommendation.newsList.length > 0 && recommendation.id !== 9 /* KEPCO, ACCORDING TO PAOLA DON'T SHOW IT */;
                                                                                                                            }
                                                                                                                        );                                                                                                    
                                                                                              });
                                                                             });
                                          }                                          
                                       },
                        metaData : {
                                       selectedMenu : 'market-insight',
                                       analyticsEventName : 'Bambu.TR.View.MarketDetail'
                                   }
                  }
             ];
});