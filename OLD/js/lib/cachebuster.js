define(['text!version.json?v='+Date.now()],function(setting)
{
    try
    {
        setting = JSON.parse(setting);
    }
    catch(e)
    {
        setting = {};
    }
    requirejs.config({urlArgs : function(id,url){
        var version = setting.version || Date.now();
        return (url.indexOf('?') === -1 ? '?':'&') + 'rv=' + version;
    }});
});