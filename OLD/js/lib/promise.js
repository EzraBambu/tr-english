/*
 * AUTHOR : MARVIN BUSTAMANTE
 * DESCRIPTION: THIS LIB SERVE TO ENCAPSULATE THE PROMISE LIB INTO SOMETHING THAT
 *              IS MUCH EASIER TO USE. ALSO THIS DOES NOT MAKE USE OF ANGULARJS
 *              PROMISE SINCE THIS MODULE CAN BE EXPORTED TO SERVER SO 
 *              DEPENDENCY ON ANGULAR IS DISCOURAGE
 *
 */
define(['q','underscore'],function(q,_)
{
    var self = {
                    pass : pass,
                    reject : reject,
                    invoke : invoke,
                    invokecb : invokecb,
                    wrap : wrap,
                    defered : defered,
                    load : load
               };

    function pass(value)
    {
        //RETURN THE VALUE AS PROMISE   
        return q.fcall(function()
        {
            return value;
        });
    }

    function reject(value)
    {
        var defered = q.defer();
        defered.reject(value);
        return defered.promise;
    }



    function invoke(fn,args)
    {
        var self = this;
        //MAKE SURE THE FUNCTION RETURN A PROMISE
        if(_.isFunction(fn))
        {
            var returnValue = undefined;
            try
            {
                if(_.isArray(args) === false)
                {
                    args = [args];
                }
                returnValue = self.wrap(fn.apply(null,args));
            }
            catch(e)
            {
                returnValue = self.reject(e);
            }
            return returnValue;
        }
        else
        {
            //THE USER JUST PASS IN A PLAIN VALUE LET'S PRETEND ITS A FUNCTION RETURN 
            return self.pass(fn);
        }
    }


    /* 
     * THIS FUNCTION WILL JUST MAKE THE PROMISE A CONSISTENT API
     * SOME LIB IS USING FAIL,CATCH OR TRY, DAM SO MANY VARIETY. THIS FUNCTION
     * WILL TRY TO HIDE THAT DETAIL
     *
     */
    function wrap(promise)
    {
        var self = this;
        if(_.isObject(promise))
        {
            if(_.isFunction(promise.then))
            {
                var defered = q.defer();

                promise.then(function(response)
                {
                    defered.resolve(response);
                });
                var rejectFunc = function(err)
                {
                    defered.reject(err);
                }
                if(_.isFunction(promise.fail) === true)
                {
                    promise.fail(rejectFunc);
                }
                if(_.isFunction(promise['catch']) === true)
                {
                    promise['catch'](rejectFunc);
                }

                return defered.promise;
            }
            else
            {
                return self.pass(promise);
            }
        }
        else
        {
            return self.pass(promise);
        }
    }



    /*
     * THIS FUNCTION WILL CONVERT THE NODE.JS CALLBACK STYLE FUNCTION CALL
     * INTO THE PROMISE STYLE
     *
     */
    function invokecb(fn,args)
    {
        var self = this;
        if(_.isFunction(fn))
        {
            var returnValue = undefined;
            try
            {
                //args = type.toArray(args);
                if(_.isArray(args) === false)
                {
                    args = [args];
                }
                var defered = q.defer();
                args.push(_callback.bind(defered)); //BIND THE FUNCTION TO THE DEFERED SO WE COULD REFERENCE IT LATER WITHOUT PASSING IT AS A PARAMETER
                returnValue = defered.promise;
                fn.apply(null,args); //INVOKE THE FUNCTION, PASSING THE CALLBACK AS THE LAST VALUE
            }
            catch(e)
            {
                returnValue = self.reject(e);
            }
            return returnValue;     
        }
        else
        {
            //THE USER JUST PASS IN A PLAIN VALUE LET'S PRETEND ITS A FUNCTION RETURN 
            return self.pass(fn);
        }
    }

    function _callback(err,response)
    {
        var defered = this;
        if(err)
        {
            defered.reject(err);
        }
        else
        {
            defered.resolve(response);
        }
    }


    function defered()
    {
        var self = this;
        return self.pass(q.defer());
    }

    function load(files)
    {
        var self = this;
        if(_.isArray(files) === false)
        {
            files = [files];
        }
        return self.defered()
                      .then(function(defered)
                      {
                            require(
                                        files,
                                        function()
                                        {
                                            var args = Array.prototype.slice.call(arguments);
                                            if(args.length == 1)
                                            {
                                                defered.resolve(args[0]);
                                            }
                                            else
                                            {
                                                defered.resolve(args);
                                            }
                                        },
                                        function(err)
                                        {
                                            defered.reject(err);
                                        }
                                   );
                            return defered.promise;
                      });
    }



    return self;
})