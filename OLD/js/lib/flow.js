/*
 * AUTHOR : MARVIN BUSTAMANTE
 * DESCRIPTION: THIS LIB SERVER TO ENCAPSULATE THE ASYNC LIB INTO SOMETHING THAT
 *              IS MUCH EASIER TO USE. ALSO THIS DOES NOT MAKE USE OF ANGULARJS
 *              PROMISE SINCE THIS MODULE CAN BE EXPORTED TO SERVER SO 
 *              DEPENDENCY ON ANGULAR IS DISCOURAGE
 *
 */

 define(['promise','async','q','underscore'],function(promise,async,q,_)
 {
    var self = {
                    waterfall : waterfall,
                    parallel : parallel,
                    each : each,
                    next : next
               };

    function waterfall(map,context)
    {
        var self = this;
        var defered = q.defer();
        var keys = _.keys(map);
        async.waterfall(
                            _waterfallProxies.apply(self,[map,keys]),
                            function(err,result)
                            {
                                if(err)
                                {
                                    defered.reject(err);
                                }
                                else
                                {
                                    defered.resolve(result);
                                }
                            }
                       );
        return defered.promise; 
    }

    function parallel(map)
    {
        var self = this;
        var defered = q.defer();    
        var keys = _.keys(map);

        async.parallel( 
                            _parallelProxies.apply(self,[map,keys]),
                            function(err,response)
                            {
                                if(err)
                                {
                                    defered.reject(err);
                                }
                                else
                                {
                                    defered.resolve(response);
                                }
                            }
                      );
        return defered.promise;
    }

    function each(list,fn)
    {
        var defered = q.defer();
        if(_.isArray(list) === false)
        {
            list = [list];
        }
        async.eachSeries(
                            list,
                            function(item,callback)
                            {
                                try
                                {
                                    var promise = fn(item);
                                    if(_.isFunction(promise.then))
                                    {
                                        promise.then(function(response)
                                                {
                                                    callback(null,response);
                                                });
                                        var catchError = function(err)
                                        {
                                            defered.reject(err);
                                        }
                                        if(_.isFunction(promise.fail))
                                        {
                                            promise.fail(catchError);
                                        }
                                        else
                                        {
                                            promise.catch(catchError);
                                        }
                                    }
                                    else
                                    {
                                        callback(null,promise);
                                    }
                                }
                                catch(e)
                                {
                                    defered.reject(e);
                                }
                            },
                            function(err,response)
                            {
                                if(err)
                                {
                                    defered.reject(err);
                                }
                                else
                                {
                                    defered.resolve(list);
                                }
                            }
                        );
        return defered.promise; 
    }


    function next(fn)
    {
        var self = this;
        var defered = q.defer();

        // DELAY EXECUTION OF THIS TASK ON THE NEXT CYCLE AND AFTER HE IO THREAD SO TO GIVE 
        // OTHER USER A FAIR CHANCE TO DO SOME WORK
        setTimeout(function()
        {
            promise.invoke(fn)
                .then(function(response)
                {
                    defered.resolve(response);
                })
                .fail(function(err)
                {
                    defered.reject(err);
                });
        },0);
        return defered.promise;
    }




    function _waterfallProxies(map,keys,context)
    {
        var self = this;    
        var functions = _.map(
                                    keys,
                                    function(key)
                                    {                                        
                                        return function(result,callback)
                                        {
                                            try
                                            {
                                                if(arguments.length == 1)
                                                {
                                                    callback = result;// THIS IS THE 1ST CALL SO ONLY CALLBACK IS AVAILABLE
                                                    result = {};
                                                }                              
                                                promise.invoke(map[key],result)
                                                       .then(function(response)
                                                        {     
                                                            // LET'S BE FAIR WITH OTHER USER AND GIVE THEM A CHANCE TO DO TASK
                                                            // BEFORE WE DO ANOTHER TASK AGAIN
                                                            return self.next(function()
                                                            {
                                                                result[key] = response;
                                                                callback(null,result);
                                                            });
                                                        })
                                                       .fail(function(err)
                                                        {
                                                            callback(err);
                                                        });                                      
                                            }
                                            catch(e)
                                            {
                                                callback(e);
                                            }
                                        }                                        
                                    }
                              ); 
        return functions;
    }


    function _parallelProxies(map,keys,context)
    {
        var proxy = {};
        _.map(
                            keys,
                            function(key)
                            {    
                                proxy[key] = function(callback)
                                {
                                    var func = map[key];
                                    try
                                    {
                                        promise.invoke(func)
                                               .then(function(response)
                                               {
                                                    callback(null,response);
                                               })
                                               .fail(function(err)
                                               {
                                                    callback(err);
                                               });
                                    }
                                    catch(e)
                                    {
                                        callback(e);
                                    }   
                                };                                                            
                            }
                        ); 
        return proxy;
    }


    return self;
 });