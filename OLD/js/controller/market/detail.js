define(['helper'],function(helper)
{
    return function($scope,recommendations,info,selectedReccomendationId)
    {
        var self = this;
        $scope.data = {                                        
                            selectedReccomendationId: selectedReccomendationId,
                            selectedRecommendation : helper.whereOne(recommendations,{id : selectedReccomendationId}),
                            recommendations : recommendations,                                        
                            news : info.news,
                            clients : info.clients
                      };

        $scope.handler = {};
        $scope.handler.getIcon = function(data)
        {
            var src = '';
            if(helper.isObject(data))
            {
                if(data.icon === 'star')
                {
                    src = 'star.png';
                }
                else
                {
                    //CIRCLE
                    src = 'circle.png';
                }
                if(src.length > 0 && helper.isString(data.effect) && data.effect.length > 0)
                {   
                    src = 'img/' + data.effect + '-' + src;
                }
                else
                {
                    console.log(data);
                }
            }
            return src;
        };

        $scope.handler.selectMarket = function(recommendation)
        {
            self.gotoAction('detail',{recommendationId : recommendation.id});
        };

        $scope.handler.onClientClick = function(client)
        {
            self.go({controller : 'client',action : 'info'},{clientId:client.id});
        };
    }  
});