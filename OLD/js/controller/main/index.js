define(['config','jquery'],function(config,$)
{
    return function($rootScope,$scope,$timeout,globalLabels,urls,eventService)
    {
        var _init = false;
        $scope.data = {
                            selectedMenu : '',
                            labels : globalLabels,
                            urls : urls,
                            isSortingVisible : false
                      };

        $scope.handler = {};
        $scope.handler.onClickSorting = function(sortField)
        {
            eventService.broadcast(config.events.sortChange,{field : sortField});
            var sortText;
            if(sortField === 'urgency')
            {
                sortText = 'By Urgency';
            }
            else
            {
                sortText = 'By Lost';
            }
            $('#sort-by').text(sortText);
        };


        $rootScope.$on(
                            'select-menu',
                            function(x,payload)
                            {
                                $scope.data.selectedMenu = payload.name;
                                var route = payload.route;
                                $scope.data.isSortingVisible = route.controller === 'client' && route.action === 'index';
                            }
                     );
    }            
});