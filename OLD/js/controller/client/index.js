define(['config','jquery','helper'],function(config,$,helper)
{
    var sortByField = {
                            urgency : 'impact.positive',
                            lost : 'impact.negative'
                      };
    return function($scope,labels,clients)
    {
        var self = this;
        $scope.data = {
                            labels : labels,
                            clients : clients                                        
                      };
        $scope.handler = {};

        $scope.handler.onClientClick = function(client)
        {
            self.gotoAction('info',{clientId : client.id});
        };


        function _sort(field)
        {
            var criteria = {};
            if(field === 'urgency')
            {
                criteria = {'impact.positive':-1,'impact.negative':-1};
            }
            else
            {
                criteria = {'impact.negative':-1,'impact.positive':-1};
            }
            helper.sortBy(clients,criteria)
                  .then(function(list)
                  {
                        $scope.data.clients = list;
                  });
        }


        $scope.$on(
                        config.events.sortChange,
                        function(e,payload)
                        {
                            payload = payload || {};
                            var fieldName = payload.field;
                            _sort(fieldName);
                        }
                  );

    }        
});