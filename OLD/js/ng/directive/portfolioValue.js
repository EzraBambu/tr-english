define([],function()
{
    return function($filter)
    {
        return {
                    restrict: 'A',
                    scope : {
                                currency : '@'
                            },
                    link: function($scope, element, attrs) 
                    {                                    
                        var value = parseFloat(attrs['portfolioValue']);
                        if(isNaN(value) === true)
                        {
                             element.replaceWith('***');  
                        }
                        else
                        {
                            var currency = $scope.currency || 'SGD';
                            var valueString = value.toString();
                            var parts = valueString.split('.');
                            if(parts.length == 1)
                            {
                                parts[1] = '00';
                            }
                            if(parts[1].length == 1)
                            {
                                parts[1] = parts[1] +  '0';
                            }
                            var html = [
                                           $filter('number')(value,0),
                                           ". <sub>" + parts[1] + ' ' + currency + "</sub>"
                                      ];
                            element.replaceWith(html.join(''));
                        }
                    }
                };
        }    

    /*return {
                type : 'directive',
                name : 'portfolioValue',
                func : function($filter)
                {
                    return {
                                restrict: 'A',
                                scope : {
                                            currency : '@'
                                        },
                                link: function($scope, element, attrs) 
                                {                                    
                                    var value = parseFloat(attrs['portfolioValue']);
                                    if(isNaN(value) === true)
                                    {
                                         element.replaceWith('***');  
                                    }
                                    else
                                    {
                                        var currency = $scope.currency || 'SGD';
                                        var valueString = value.toString();
                                        var parts = valueString.split('.');
                                        if(parts.length == 1)
                                        {
                                            parts[1] = '00';
                                        }
                                        if(parts[1].length == 1)
                                        {
                                            parts[1] = parts[1] +  '0';
                                        }
                                        var html = [
                                                       $filter('number')(value,0),
                                                       ". <sub>" + parts[1] + ' ' + currency + "</sub>"
                                                  ];
                                        element.replaceWith(html.join(''));
                                    }
                                }
                            };
                }
            }*/
});