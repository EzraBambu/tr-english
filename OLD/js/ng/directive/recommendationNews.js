define(['jquery','helper','text!view/shared/client/recommended-news/news.html'],function($,helper,template)
{
    return function($compile,newsRepository)
    {
         return {
                    restrict: 'E',
                    require: 'ngModel',
                    transclude: true,
                    replace: false,
                    template : template,
                    scope : {
                                ngModel: '=?',
                            },
                    link: function($scope, $element, attrs,ngModel) 
                    {
                        $scope.data = {
                                            news : $scope.ngModel
                                      };
                        $scope.handler = {};
                        $scope.handler.getIcon = function(data)
                        {
                            var src = '';                        

                            if(helper.isObject(data))
                            {
                                if(data.icon === 'star')
                                {
                                    src = 'star.png';
                                }
                                else
                                {
                                    //CIRCLE
                                    src = 'circle.png';
                                }
                                if(src.length > 0 && helper.isString(data.effect) && data.effect.length > 0)
                                {   
                                    src = 'img/' + data.effect + '-' + src;
                                }
                                else
                                {
                                    console.log(data);
                                }
                            }
                            return src;
                        }

                        $scope.handler.onClickRelatedNewss = function(news)
                        {
                            $scope.data.news = news;
                        }

                    }
                };
    }
    /*return {
                type : 'directive',
                name : 'recommendationNews',
                func : function($compile,newsRepository)
                {
                     return {
                                restrict: 'E',
                                require: 'ngModel',
                                transclude: true,
                                replace: false,
                                template : template,
                                scope : {
                                            ngModel: '=?',
                                        },
                                link: function($scope, $element, attrs,ngModel) 
                                {
                                    $scope.data = {
                                                        news : $scope.ngModel
                                                  };
                                    $scope.handler = {};
                                    $scope.handler.getIcon = function(data)
                                    {
                                        var src = '';                        

                                        if(helper.isObject(data))
                                        {
                                            if(data.icon === 'star')
                                            {
                                                src = 'star.png';
                                            }
                                            else
                                            {
                                                //CIRCLE
                                                src = 'circle.png';
                                            }
                                            if(src.length > 0 && helper.isString(data.effect) && data.effect.length > 0)
                                            {   
                                                src = 'img/' + data.effect + '-' + src;
                                            }
                                            else
                                            {
                                                console.log(data);
                                            }
                                        }
                                        return src;
                                    }

                                    $scope.handler.onClickRelatedNewss = function(news)
                                    {
                                        $scope.data.news = news;
                                    }

                                }
                            };
                }
           };*/
});