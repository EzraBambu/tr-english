define(['helper','jquery'],function(helper,$)
{
    return function()
    {       
        return {
                    restrict: 'E',
                    scope : false,
                    link: function($scope, element, attrs) 
                    {                        
                        if(attrs.ngClick || attrs.href === '' || attrs.href === '#')
                        {
                            element.on('click', function(e){                                            
                                e.preventDefault();
                            });
                        }                                
                    }
                }; 
    };   
})