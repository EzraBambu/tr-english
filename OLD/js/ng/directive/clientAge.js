define(['helper'],function(helper)
{
    return function()
    {
         return {
                    restrict: 'A',
                    require: 'ngModel',
                    scope : {
                                ngModel: '=?',
                            },
                    link: function($scope, element, attrs,ngModel) 
                    {
                        var model = $scope.ngModel;
                        if(helper.isObject(model) && helper.isString(model.dob) && model.dob.length > 0)
                        {
                            var parts = model.dob.split('-');
                            var currentYear = (new Date()).getFullYear()
                            var dobYear = parseInt(parts[0]);
                            if(helper.isNumber(dobYear))
                            {
                                element.text(currentYear - dobYear);
                            }
                        }
                    }
                };
    }
    /*return {
                type : 'directive',
                name : 'clientAge',
                func : function()
                {
                     return {
                                restrict: 'A',
                                require: 'ngModel',
                                scope : {
                                            ngModel: '=?',
                                        },
                                link: function($scope, element, attrs,ngModel) 
                                {
                                    var model = $scope.ngModel;
                                    if(helper.isObject(model) && helper.isString(model.dob) && model.dob.length > 0)
                                    {
                                        var parts = model.dob.split('-');
                                        var currentYear = (new Date()).getFullYear()
                                        var dobYear = parseInt(parts[0]);
                                        if(helper.isNumber(dobYear))
                                        {
                                            element.text(currentYear - dobYear);
                                        }
                                    }
                                }
                            };
                }
           };*/
});