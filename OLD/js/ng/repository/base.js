define(['promise','underscore','mingo'],function(promise,_)
{    
    var _cacheByTableName = {};
    return function(proxyService)
    {
        /* MINGO SETUP */
        Mingo.setup({
                        key: 'id' // default
                    });

        var BaseRepository = function(name)
        {
            this.tableName = name;
        };



        BaseRepository.prototype.find = function(filter)
        {
            var self = this;     
            var records = _cacheByTableName[self.tableName];

            if(records === undefined)
            {
                records = proxyService.get(self.tableName + '.json');
                _cacheByTableName[self.tableName] = records;
            }                        
            return records.then(function(data)
            {                            
                return _filter.call(self,data,filter);
            });
        };

        BaseRepository.prototype.findById = function(id)
        {
            var self = this;
            if(_.isNumber(id) === false)
            {                            
                id = parseInt(id);
            }
            return self.findOne({id : id});
        }

        BaseRepository.prototype.findOne = function(filter)
        {
            var self = this;
            return self.find(filter)
                       .then(function(records)
                       {
                            if(records.length == 0)
                            {
                                return {};
                            }  
                            else
                            {
                                return records[0];
                            }
                       });
        }

        BaseRepository.prototype.save = function()
        {
        };        


        function _filter(records,filter)
        {
            var self = this;
            if(_.isObject(filter))
            {
                try
                {
                    var query = new Mingo.Query(filter);
                    records = query.find(records).all();
                }
                catch(e)
                {
                    records = promise.reject(e);
                }
            }                                        
            return records;
        }

        return BaseRepository;
    }

    /*return {
                type : 'factory',
                func : function(proxyService)
                {
                    Mingo.setup({
                                    key: 'id' // default
                                });

                    var BaseRepository = function(name)
                    {
                        this.tableName = name;
                    };



                    BaseRepository.prototype.find = function(filter)
                    {
                        var self = this;     
                        var records = _cacheByTableName[self.tableName];

                        if(records === undefined)
                        {
                            records = proxyService.get(self.tableName + '.json');
                            _cacheByTableName[self.tableName] = records;
                        }                        
                        return records.then(function(data)
                        {                            
                            return _filter.call(self,data,filter);
                        });
                    };

                    BaseRepository.prototype.findById = function(id)
                    {
                        var self = this;
                        if(_.isNumber(id) === false)
                        {                            
                            id = parseInt(id);
                        }
                        return self.findOne({id : id});
                    }

                    BaseRepository.prototype.findOne = function(filter)
                    {
                        var self = this;
                        return self.find(filter)
                                   .then(function(records)
                                   {
                                        if(records.length == 0)
                                        {
                                            return {};
                                        }  
                                        else
                                        {
                                            return records[0];
                                        }
                                   });
                    }

                    BaseRepository.prototype.save = function()
                    {
                    };        


                    function _filter(records,filter)
                    {
                        var self = this;
                        if(_.isObject(filter))
                        {
                            try
                            {
                                var query = new Mingo.Query(filter);
                                records = query.find(records).all();
                            }
                            catch(e)
                            {
                                records = promise.reject(e);
                            }
                        }                                        
                        return records;
                    }

                    return BaseRepository;
                }
            }*/
});