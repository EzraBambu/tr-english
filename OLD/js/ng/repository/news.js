define(['helper','flow'],function(helper,flow)
{
    return function(BaseRepository)
    {
        function Repository()
        {
            BaseRepository.apply(this,Array.prototype.slice.call(arguments));
        }
        helper.inherits(Repository,BaseRepository);

        Repository.prototype.getByClientId = function(clientId)
        {
            var self = this;
            if(helper.isString(clientId))
            {
                clientId = [parseInt(clientId)];
            }
            if(helper.isArray(clientId) === false)
            {
                clientId = [clientId];
            }                                
            return self.find({client : {$in : clientId} })
                       .then(function(result)
                       {                                        
                            return result;
                       });
        }

        Repository.prototype.getNews = function(ids)
        {
            var self = this;
            return self.find({id : {$in : ids}})
                       .then(function(list)
                       {
                            return flow.each(
                                                helper.clone(list),
                                                function(news)
                                                {
                                                    if(helper.isArray(news.relatedNews))
                                                    {

                                                        return self.getNews(news.relatedNews)
                                                                   .then(function(relatedNews)
                                                                   {
                                                                        news.relatedNews = relatedNews;
                                                                        return news;
                                                                   });
                                                        /*var collectedRelatedNews = [];
                                                        return flow.each(
                                                                            news.relatedNews,
                                                                            function(relatedNews)
                                                                            {
                                                                                if(helper.isObject(relatedNews))
                                                                                {
                                                                                    collectedRelatedNews.push(relatedNews);
                                                                                    return relatedNews;
                                                                                }
                                                                                else
                                                                                {
                                                                                    return self.getNews(relatedNews)
                                                                                               .then(function(response)
                                                                                               {
                                                                                                    //console.log(response);
                                                                                                    collectedRelatedNews.concat(response);
                                                                                                    return response;
                                                                                               });
                                                                                }
                                                                            }
                                                                        )
                                                                    .then(function(response)
                                                                    {
                                                                        console.log(collectedRelatedNews);
                                                                        return response;
                                                                    });*/
                                                    }
                                                    else
                                                    {
                                                        return news;
                                                    }
                                                }
                                            );
                       });
        }

        var self = new Repository('news');
        return self;
    }
    /*return {
                type : 'factory',
                func : function(BaseRepository)
                {
                    function Repository()
                    {
                        BaseRepository.apply(this,Array.prototype.slice.call(arguments));
                    }
                    helper.inherits(Repository,BaseRepository);

                    Repository.prototype.getByClientId = function(clientId)
                    {
                        var self = this;
                        if(helper.isString(clientId))
                        {
                            clientId = [parseInt(clientId)];
                        }
                        if(helper.isArray(clientId) === false)
                        {
                            clientId = [clientId];
                        }                                
                        return self.find({client : {$in : clientId} })
                                   .then(function(result)
                                   {                                        
                                        return result;
                                   });
                    }

                    Repository.prototype.getNews = function(ids)
                    {
                        var self = this;
                        return self.find({id : {$in : ids}});
                    }

                    var self = new Repository('news');
                    return self;
                }
           };*/
});