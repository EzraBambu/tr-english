define(['config','flow','promise','underscore','jquery'],function(config,flow,promise,_,$)
{
    var jsConfig = requirejs.s.contexts._.config.urlArgs;
    var version;
    if(_.isFunction(jsConfig))
    {
        try
        {
            version = jsConfig('_','');
            version = version.replace('?rv=','');
        }
        catch(e)
        {
            version = undefined;
        }
    }
    version = version || Date.now();

    var self = {};
    var appVersion = version;
    var _reflectionCache = {};
    var _registerByType = {
                                directive :{},
                                factory : {},
                                repository : {},
                                service : {}
                          };
    var _routeMapping = {};
    var _routeByName = {};    
    var _routeByIds = {};
    var _routeMain = undefined;


    self.inherits = function(child,parent)
    {
        if(typeof child === 'function')
        {            
            var parentInstance = Object.create(parent.prototype);
            child.prototype = parentInstance;
            child.prototype.constructor = child;
            child.prototype.base = new parent();
        }
    };

    self.extend = function(destination,source)
    {
        return _.extend(_.extend({},destination),source);
    };

    self.each = function(list,fn)
    {
        return _.forEach(list,fn);
    }

    self.isString = function(value)
    {
        return _.isString(value);
    }

    self.isArray = function(value)
    {
        return _.isArray(value);
    }

    self.isObject = function(value)
    {
        return _.isObject(value);
    }    

    self.isNumber = function(value)
    {
        return _.isNumber(value);
    }

    self.isFunction = function(value)
    {
        return _.isFunction(value);
    }   

    self.map = function(list,fn)
    {
        return _.map(list,fn);
    };

    self.keys = function(value)
    {
        return _.keys(value);
    }   


    self.fnArgs = function(fn)
    {
        var info = _getFn(fn);
        if(info)
        {
            if(typeof info.args == 'undefined')
            {
                info.args = info.code.match (/function\s*\w*\s*\((.*?)\)/)[1].split (/\s*,\s*/);
            }
        }        
        return info.args;
    };

    self.fnName = function(fn)
    {
        var info = _getFn(fn);
        var name = '';
        
        if(info)
        {
            if(typeof info.name === 'undefined')
            {
                name = info.code.substr('function '.length);
                name = name.substr(0, name.indexOf('('));
                info.name = name;
            }
            else
            {
                name = info.name;
            }
        }            
        return name;
    };

    self.getService = function(name)
    {
        var app = require('app');
        return app._globals.$injector.get(name);
    }

    self.register = function(definition)
    {
        var name = definition.name;        
        if(definition.type === 'directive')
        {
            return _registerDirective(definition);
        }
        else if(definition.type === 'factory')
        {
            return _registerFactory(definition);
        }
        else
        {
            console.log('ERROR NO REGISTRATION');
            return promise.pass(definition);
        }
    };

    self.clone = function(value)
    {
        if(_.isObject(value))
        {
            return JSON.parse(JSON.stringify(value));
        }
        else
        {
            return value;
        }
    }

    self.pluck = function(list,propertyName)
    {
        return _.pluck(list,propertyName);
    }

    self.loadRouteUrl = function(route)
    {
        return self.defered()
                   .then(function(defered)
                   {
                        return defered.promise;
                   });
    };

    self.loadRouteAndGo = function($stateProvider,$injector,url)
    {
        if(url)
        {
            var appName;
            var routeName;
            var controller;        
            var parts = url.split('/');
            if(parts.length >= 3)
            {
                appName = parts[1];
                routeName = controllerName = parts[2];
                promise.load('route/' + routeName)
                       .then(function(routes)
                       { 
                            //PATCH THE CONTROLLER NAME
                            self.each(
                                        routes,
                                        function(route)
                                        {
                                            route = self.routeEnrichment(route);
                                            route.controller = route.controller || controllerName;
                                        }
                                     );
                           self.initRoute($stateProvider,routes);
                           self.loadDependencies(['routeService','notificationService'])
                                  .then(function()
                                  {                   
                                      var routeService = $injector.get('routeService');
                                      var route = routeService.getMatchRouteByUrl(url,routes);
                                      if(route)
                                      {
                                            routeService.go(route.route.name,route.payload);
                                      }
                                      else
                                      {
                                            //URL DOES NOT MATCH ANY ROUTE
                                            var notificationService = $injector.get('notificationService'); //FORCE LOAD OF THE NG SERVICE
                                            var $rootScope = $injector.get('$rootScope');
                                            $rootScope.$broadcast(config.events.routeNotFound,{route : routeName});
                                            routeService.goToLandingPage();
                                      }
                                  });                                                      
                       })
                       .fail(function(err)
                       {
                            //FAIL TO LOAD THE ROUTE, IT COULD ONLY MEAN THAT ITS NOT VALID
                            self.loadDependencies(['notificationService','routeService']) //MAKE SURE THE notifcationService is loaded before abt event annoucement is made
                                .then(function()
                                {
                                    var notificationService = $injector.get('notificationService');
                                    var routeService = $injector.get('routeService');
                                    var $rootScope = $injector.get('$rootScope');
                                    $rootScope.$broadcast(config.events.routeNotFound,{route : routeName});
                                    routeService.goToLandingPage();
                                });                           
                       });
            }
            else
            {                
                self.loadDependencies(['routeService','notificationService'])
                    .then(function()
                    {
                        var notificationService = $injector.get('notificationService'); //FORCE LOAD OF THE NG SERVICE
                        var $rootScope = $injector.get('$rootScope');
                        $rootScope.$broadcast(config.events.routeNotFound,{route : routeName});
                        var routeService = $injector.get('routeService');
                        routeService.goToLandingPage();
                    });
            }
        }
        else
        {
           self.loadDependencies(['routeService','notificationService'])
               .then(function()
                {
                    var notificationService = $injector.get('notificationService'); //FORCE LOAD OF THE NG SERVICE
                    var $rootScope = $injector.get('$rootScope');
                    if(url)
                    {
                        //ONLY BROADCAST STATE NOT FOUND IF IT'S NOT AN EMTPTY ARRAY
                        $rootScope.$broadcast(config.events.routeNotFound,{route : routeName});
                    }
                    var routeService = $injector.get('routeService');
                    routeService.goToLandingPage();
                })
               .fail(function(e)
               {
                    console.log(e);
               });
        }
    }

    self.loadRoute = function($stateProvider)
    {
         _.forEach(
                    config.ng.route,
                    function(file)
                    {
                        var routes = require(file);
                        self.initRoute($stateProvider,routes);
                    }
                  );
    }

    self.initRoute = function($stateProvider,routes)
    {      
        var app = require('app');
        if(app._routes === undefined)
        {
            app._routes = [];
        }
        _.forEach(
                    routes,
                    function(record)
                    {
                        record = self.routeEnrichment(record);
                        if(_routeByIds[record.id] === undefined)
                        {
                            var route = _routeMap(record);
                            if(route.controller !== 'main')
                            {
                                route.name = _routeMain.name + '.' + route.name;
                                if(route.isSystem === undefined || route.isSystem === false)
                                {
                                    route.dependencies.url ='/' + route.controller + route.dependencies.url;
                                }
                            }
                            _routeByName[route.name] = route;
                            app._routes.push(route);
                            _routeByIds[record.id] = route;// MARK THE ROUTE AS REGISTERED

                            //REGISTER TO NG ROUTE
                            route.gotoAction = _gotoAction;
                            route.go = _go;
                            $stateProvider.state(route.name,_createRoute(route));                            
                        }
                    }
                 ); 
        app._routeByName = _routeByName; 
    }

    self.routeEnrichment = function(route)
    {
        route = route || {};
        route.id = route.id || _generateRouteId();
        route.action = route.action || 'index';
        route.dependencies = route.dependencies || {};
        route.dependencies.url = route.dependencies.url || '';              
        route.name = route.name || _generateRouteName(); //LET'S GET LAZY, IF NO NAME IS PROVIDED SINCE NAME SHOULD BE IMMATERIAL THEN LETS GENERATE SOME RANDOM NAME
        route.metaData = route.metaData || {};
        return route;
    }

    self.sortBy = function(list,criteria)
    {
        return promise.defered()
                      .then(function(defered)
                      {
                            if(self.isArray(list) && self.isObject(criteria))
                            {
                                var query = new Mingo.Query({});
                                var records;
                                try
                                {
                                    records = query.find(list).sort(criteria).all();
                                }
                                catch(e)
                                {
                                    console.log(e);
                                    records = [];
                                }
                                list = records;
                            }
                            defered.resolve(list);
                            return defered.promise;
                      });
        
    };

    self.where = function(list,criteria)
    {
        return _.where(list,criteria);
    }

    self.whereOne = function(list,criteria)
    {
        return self.where(list,criteria)[0];
    }

    self.filter = function(list,predicate)
    {
        return _.filter(list,predicate);
    };

    self.loadDependencies = _loadDependencies;

    function _createRoute(route)
    {       
        var controller = route.controller;
        var action = route.action;
        var optionalDependencies = route.dependencies;

        var routeDef = {};        
        var controllerName = controller.replace('/','') + action + 'Controller';
        routeDef.templateUrl = config.js.view + '/' + controller +'/' + action + '.html?nv=' + appVersion;
        routeDef.controller = controllerName;
        routeDef.resolve = {
                                load: ['$q', '$rootScope', function ($q, $rootScope)
                                {       
                                    return _define(
                                                        'controller',
                                                        controllerName,
                                                        function()
                                                        {
                                                            return _ngControllerLoad.call(self,route,routeDef,$q,$rootScope);
                                                        }
                                                   );                                                                    
                                }]
                           };

        //TODO : LOAD OPTIONAL ROUTE DEPENDENCIES
        if(typeof optionalDependencies === 'object')
        {
            _.keys(optionalDependencies)
             .forEach(function(key)
             {                
                var dep = optionalDependencies[key];
                if(typeof dep === 'function' )
                {
                    dep = {func : dep};
                    _resolveRouteDependencies(key,routeDef,dep);
                }                
                else if(_.isArray(dep))
                {
                    if(key === 'directive')
                    {
                        //LOAD THE DIRECTIVE
                        routeDef[key] = _loadDirective(dep);
                    }
                    else if(key === 'route')
                    {                        
                        routeDef.resolve[key] = [
                                                    'routeService',
                                                    function(routeService)
                                                    {
                                                        return flow.each(
                                                                            dep,
                                                                            function(routeFile)
                                                                            {
                                                                                return routeService.load(routeFile);
                                                                            }
                                                                        );                                                                    
                                                    }
                                                ];
                    }
                    else if(key === 'filter')
                    {
                    }
                    else
                    {
                        routeDef.resolve[key] = dep;   
                    }
                }
                else if(_.isObject(dep))
                {
                    _resolveRouteDependencies(key,routeDef,dep);
                }                    
                else
                {
                    routeDef[key] = dep;
                }
             });
        }

        if(controller !== 'main')
        {
            var isSecured = true;
            if(_.isObject(route.metaData) && _.isArray(route.metaData.roles))
            {
                if(route.metaData.roles.indexOf('public') >= 0)
                {
                    isSecured = false;
                }
            }
            if(isSecured === true)
            {
                routeDef.resolve['_isAuthenticated'] = [ '$state',
                                                        function($state)
                {
                    return true;
                }];
            }
        }       
        return routeDef;
    }


    function _ngControllerLoad(route,routeDef,$q, $rootScope)
    {                        
        return promise.defered()
                      .then(function(defered)
                      { 
                            var controller = route.controller;
                            var action = route.action;    
                            var controllerFile = [config.js.controller + '/' + controller + '/' + action  + '.js'];
                            promise.load(controllerFile)
                                   .then(function(controllerResource)
                                   {                                                                                
                                        if(self.isFunction(controllerResource))
                                        {
                                            controllerResource = { func : controllerResource };
                                        }
                                        var controllerFunc = controllerResource.func;
                                        var deps = controllerResource.deps;
                                        if(deps === undefined)
                                        {
                                            deps = self.filter( 
                                                                    self.fnArgs(controllerFunc),
                                                                    function(depName)
                                                                    {
                                                                        return depName;
                                                                    }
                                                                );
                                        }
                                        //LOAD THE CONTROLLER DEPENDENCIES BEFOE REGISTRATION OF NG CONTROLLER, 
                                        //DEPS NEEDED TO BE LOADED FIRST SO THAT THE ANGULAR WILL NOT CONPLAIN ON ANY UNRESOLVED DEPS\
                                        deps.push('routeService'); //SAVE A REFERENCE TO ROUTE SERVICE SO WE COULD GET A HOLD OF IT LATER

                                        _loadDependencies(deps).then(function()
                                                                {
                                                                    var controllerFunctionProxy = function()
                                                                    {
                                                                        var args = Array.prototype.slice.call(arguments);
                                                                        var routeService = args[args.length-1];
                                                                        route.routeService = routeService;
                                                                        controllerFunc.apply(route,args);
                                                                    };
                                                                    controllerFunctionProxy.$inject = deps;

                                                                    //REGISTER THE CONTROLLER
                                                                    var app = require('app');
                                                                    app.register.controller(
                                                                                                routeDef.controller,
                                                                                                controllerFunctionProxy
                                                                                            );
                                                                    defered.resolve(controllerResource);
                                                                })
                                                                .fail(function(err)
                                                                {
                                                                    defered.reject(err);
                                                                })
                                                                .finally(function()
                                                                {
                                                                    $rootScope.$apply();
                                                                });                                                                 
                                   })
                                   .fail(function(err)
                                   {
                                        defered.reject(err);
                                   });
                            return defered.promise;
                      });
    }

    function _loadDirective(deps)
    {
        if(deps.length > 0)
        {
            var basePath = config.ng.basePath;
            var paths = _.map(
                                deps,
                                function(directiveFile)
                                {
                                    return basePath + '/directive/' + directiveFile;
                                }
                             );
            return promise.load(paths)
                          .then(function(definitions)
                          {     
                                //FORMULATE THE OBJECT SO THAT WE COULD EASILY LOAD LATER 
                                if(self.isArray(definitions) === false)
                                {
                                    definitions = [definitions];
                                }
                                var list = self.map(
                                                        definitions,
                                                        function(def,index)
                                                        {
                                                            if(self.isFunction(def))
                                                            {
                                                                def = {func : def};
                                                            }                                                                                                                
                                                            if(def.type === undefined)
                                                            {
                                                                def.type = 'directive';
                                                            }
                                                            if(def.name === undefined)
                                                            {
                                                                def.name = deps[index];
                                                            }
                                                            if(def.deps === undefined)
                                                            {
                                                                //THIS HAPPEN IF IT'S NOT COMPILED YET
                                                                def.deps = _.filter(
                                                                                        self.fnArgs(def.func),
                                                                                        function(arg)
                                                                                        {
                                                                                            return arg;
                                                                                        }
                                                                                     );
                                                            }
                                                            return def;
                                                        }
                                                   );
                                return flow.each(
                                                    list,
                                                    function(directive)
                                                    {
                                                        return _registerDirective(directive);
                                                    }
                                                );                          
                          });
        }
        else
        {
            return promise.pass();
        }        
    }

    /*function _loadRouteRouteDependencies(deps)
    {
        return flow.each(
                                deps,
                                function(routeFile)
                                {
                                     console.log('LOAD : ' + routeFile);
                                    return promise.pass();
                                }
                             )
                        .then(function()
                        {
                            console.log('DONE LOADING');
                        });
    }*/


    function _resolveRouteDependencies(key,routeDef,dep)
    {
        var routeDeps = dep.deps;
        if(routeDeps === undefined)
        {
            routeDeps = _.filter(
                                    self.fnArgs(dep.func),
                                    function(arg)
                                    {
                                        return arg;
                                    }
                                );
        }
        if(routeDeps.length > 0)
        {
            var nativeNgDeps = ['$injector']; //THIS WILL HELP US TO GET ALL THE NG DEPENDENCIES LATER AT THE PROXY SIDE
            _.forEach(
                        routeDeps,
                        function(name)
                        {
                            if(name.substring(0,1) == '$')
                            {
                                nativeNgDeps.push(name);
                            }
                        }
                     );
            var proxyFunc = function($injector)
            {
                var context = this;
                var proxyArgs = Array.prototype.slice.call(arguments);
                return _loadDependencies(routeDeps).then(function(loadedDeps)
                                            {
                                                var deps = undefined;
                                                if(proxyArgs.length > 1)
                                                {
                                                    //deps
                                                    deps = proxyArgs.slice(1);
                                                }
                                                else
                                                {
                                                    deps = [];
                                                }
                                                _.forEach(
                                                            loadedDeps,
                                                            function(loadedDep)
                                                            {
                                                                deps.push($injector.get(loadedDep.name));
                                                            }
                                                         );
                                                //CALL THE ORIGINAL FUNCTION BUT MAKE IT LOOK LIKE IT'S THE ORIGINAL CALL
                                                var response;
                                                try
                                                {
                                                    response =  dep.func.apply(context,deps);
                                                }
                                                catch(e)
                                                {
                                                    _loadDependencies(['notificationService']).then(function()
                                                                                               {
                                                                                                    var notificationService = $injector.get('notificationService');
                                                                                                    notificationService.error(e);
                                                                                               })
                                                                                               .fail(function(e)
                                                                                               {
                                                                                                    //THERE NOTHING I CAN DO SINCE EVEN THE NOTIFICATION SERVICE FAILED !!!??
                                                                                                    console.log(e);
                                                                                                    throw e;
                                                                                                    return e;                                                                                                    
                                                                                               });
                                                    response = promise.reject(e);
                                                }
                                                return response;
                                            });
            }
            nativeNgDeps.push(proxyFunc);
            //THERE IS A DEPENDENCY, LET'S CREATEA FUNCTION PROXY THAT WILL BE USED LATER TO RESOLVE ALL THE DEPENDENCIES
            routeDef.resolve[key] = nativeNgDeps;
        }
        else
        {
            //NO DEPENDENCIES JUST CARRY ON
            routeDef.resolve[key] =  [dep.func];
        }
    }

    function _routeMap(route)
    {
        if(_.isObject(route.dependencies) &&  _.isString(route.dependencies.url))
        {
            var parameters = route.dependencies.url.match(/\{[a-zA-Z ]{2,30}\}/g);
            if(_.isArray(parameters))
            {
                var count = parameters.length;
                for(var i=0;i<count;i++)
                {
                    var fieldName = parameters[i];
                    parameters[i] = fieldName.replace('{','')
                                             .replace('}','');
                }
            }
            else
            {
                parameters = [];
            }
            route.parameters = parameters;
            var routeKey = (route.controller +'-' + route.action).toLowerCase();
            //SAVE FOR URL ROUTING RESOLUTION SERVICE
            _routeMapping[routeKey] = route;

            if(typeof _routeMain === 'undefined')
            {
                if(route.controller === 'main' && route.action === 'index')
                {
                    _routeMain = route;
                }
            }
        }
        return route;
    }


    function _registerDirective(definition)
    {        
        return promise.defered()
                      .then(function(defered)
                      {
                            var name = definition.name;
                            var deps = definition.deps;
                            if(deps === undefined)
                            {
                                //SINCE THE USER DID NOT DEFINE THE INJECTION, LETS DETECT THE DEPENDENCY BY CHECKING THE FUNCTION ARGUMENTS
                                deps = _.filter(
                                                    self.fnArgs(definition.func),
                                                    function(arg)
                                                    {
                                                        return arg;
                                                    }
                                                 );
                            }
                            else
                            {
                                deps = self.clone(deps);
                            }
                            var directiveDeps = definition.depsDirective || [];
                            _loadDirective(directiveDeps).then(function()
                            {     
                                if(_isDefinedDirective(name) === false)
                                {
                                    var app = require('app'); //GET A REFERNCE TO NG MODULE
                                    deps.push(definition.func); //SAVE THE FUNCTION TO THE DEPS THIS IS NG SPECIFIC FORMAT
                                    app.register.directive(name,deps); //NG DIRECTIVE REGISTRATION
                                    _registerByType['directive'][name] = definition; //MARK THE DIRECTIVE AS LOADED
                                }
                                defered.resolve(definition);
                            });                                                                                
                            return defered.promise;
                      });
    }

    function _registerFactory(definition)
    {        
        var name = definition.name;
        var deps = definition.deps;
        
        return promise.defered()
                      .then(function(defered)
                      {                         
                            if(deps === undefined)
                            {
                                //SINCE THE USER DID NOT DEFINE THE INJECTION, LETS DETECT THE DEPENDENCY BY CHECKING THE FUNCTION ARGUMENTS
                                deps = _.filter(
                                                    self.fnArgs(definition.func),
                                                    function(arg)
                                                    {
                                                        return arg;
                                                    }
                                                 );                                
                            }
                            _loadDependencies(deps).then(function()
                            {
                               if(_isDefined(definition.type,name) === false)
                               {
                                    var app = require('app');
                                    //app.register.factory(name,definition.func);
                                    var factoryDef = [].concat(deps);
                                    factoryDef.push(definition.func);
                                    app.register.factory(name, factoryDef);
                                    _registerByType[definition.type][name] = definition;
                               }
                               defered.resolve(definition);
                            });                           
                            return defered.promise;
                      });
        
    }

    function _loadDependencies(deps)
    {
        var dependencies =  _getResolveableDependencies(deps);
        var unResolvedDependencies = self.where(dependencies,{defined : false});
        if(unResolvedDependencies.length > 0)
        {
            return promise.load(self.pluck(unResolvedDependencies,'path'))
                          .then(function(definitions)
                          {
                                if(self.isArray(definitions) === false)
                                {
                                    definitions = [definitions];
                                }
                                var list = self.map(
                                                        definitions,
                                                        function(def,index)
                                                        {
                                                            //MAKE AN ADJUSTMENT BASE ON THE TYPE
                                                            if(self.isFunction(def))
                                                            {
                                                                def = {func : def};   
                                                            }
                                                            if(def.type === undefined)
                                                            {
                                                                def.type = 'factory';
                                                            }
                                                            def.name = unResolvedDependencies[index].name;
                                                            return def;
                                                        }
                                                   );
                                return flow.each(
                                                    list,
                                                    function(def)
                                                    {
                                                        return self.register(def)
                                                                   .then(function()
                                                                   {
                                                                        return def;
                                                                   });
                                                    }
                                                );
                          })
                          .fail(function(err)
                          {
                                console.log(err);
                                return err;
                          });
        }
        else
        {
            return promise.pass(dependencies);
        }
    }

    function _define(typeName,resourceName,fn)
    {
        if(_isDefined(typeName,resourceName) === false)
        {
            return promise.invoke(fn)
                          .then(function(response)
                          {
                                response = response || true; //MAKE SURE THE FUNCTION RETURN SOMETHING SO THAT WE CAN DETERMINED IF IFS CACHE LATER
                                _registerByType[typeName][resourceName] = response;
                                return response;
                          });
        }
        else
        {
            return promise.pass(_registerByType[typeName]);
        }
    }

    function _isDefined(typeName,resourceName)
    {        
        if(_registerByType[typeName] === undefined)
        {
            _registerByType[typeName] = {};
        }
        return _registerByType[typeName][resourceName] !== undefined
    }

    function _isDefinedDirective(resourceName)
    {        
        return _isDefined('directive',resourceName);
    }

    function _getResolveableDependencies(deps)
    {
        var dependencies = [];
        var suffixes = config.ng.suffixes;
        var ngBasePath = config.ng.basePath;
        _.forEach(
                    deps,
                    function(resourceName,index)
                    {
                        _.forEach(
                                        suffixes,
                                        function(suffix)
                                        {
                                            var index = resourceName.lastIndexOf(suffix);                                            
                                            if(index > 0)
                                            {
                                                var typeName = suffix.toLowerCase(); 
                                                var rootName = resourceName.substring(0,index);                                                
                                                dependencies.push(
                                                                    {
                                                                        type : typeName,
                                                                        name : resourceName,
                                                                        path : (ngBasePath +'/' + typeName + '/' + rootName),
                                                                        defined : _isDefined(typeName,resourceName)
                                                                    }
                                                                 );                                                
                                            }                                           
                                        }
                                 );
                    }
                 );
        return dependencies;
    }


    function _getFn(fn)
    {
        if(typeof fn === 'function')
        {
            if(typeof _reflectionCache[fn] === 'undefined')
            {
                _reflectionCache[fn] = {
                                            code : fn.toString(),
                                            name : undefined,
                                            args : undefined
                                        };
            }
        }
        return _reflectionCache[fn];
    } 

    function _generateRouteId()
    {
        var id;
        while(true)
        {
            id =  Math.ceil(Math.random()*9999999999);
            if(_routeByIds[id] === undefined)
            {
                break;
            }
        }
        return id;
    }

    function _generateRouteName()
    {
        var name;
        while(true)
        {
            name = 'route-' + Math.ceil(Math.random()*9999999999);
            if(_routeByName[name] === undefined)
            {
                break;
            }
        }
        return name;
    }


    /*
     * CONTROLLER ENRICHMENT
     * THIS FUNCTION WILL AUTOMATE RESOLVING THE CONTROLLER NAME SO THAT YOU DON'T HAVE TO PASS IT
     */
    function _gotoAction(action,payload)
    {
        var self = this;        
        self.go({controller:self.controller,action:action},payload);
    }

    function _go(routeDef,payload)
    {        
        var self = this;
        payload = payload || {};        
        self.routeService.go(routeDef,payload);
    }




    return self;
})