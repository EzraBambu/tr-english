define(['jquery','helper','text!view/shared/recommended-news-list.html'],function($,helper,recommendedNewsListTemplate)
{    
    return {
                depsDirective : ['a','recommendationNewsSummary','recommendationNews'],
                func : function($compile)
                {
                     return {
                                restrict: 'E',
                                require: 'ngModel',
                                transclude: true,
                                replace: false,
                                template : recommendedNewsListTemplate,
                                scope : {
                                            ngModel: '=?',
                                        },
                                link: function($scope, $element, attrs,ngModel) 
                                {
                                    $scope.data = {
                                                        news : $scope.ngModel
                                                  };

                                    $scope.handler = {};
                                    $scope.$watch(
                                                    'ngModel',
                                                    function(news,oldList)
                                                    {
                                                        if(news !== oldList)
                                                        {
                                                            $scope.data.news = news;
                                                        }
                                                    }
                                                 );
                                }
                            };
                }
           };
});