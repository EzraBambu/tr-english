define(['helper'],function(helper)
{
    return function(settingRepository)
    {
         return {
                    restrict: 'A',
                    require: 'ngModel',
                    scope : {
                                ngModel: '=?',
                            },
                    link: function($scope, element, attrs,ngModel) 
                    {
                        var model = $scope.ngModel;
                        if(helper.isObject(model) && helper.isObject(model.portfolio) && helper.isString(model.portfolio.assessmentType))
                        {
                           var typeText = model.portfolio.assessmentType;
                           settingRepository.getAsessmentTypes()
                                            .then(function(types)
                                            {
                                                typeText = types[model.portfolio.assessmentType];
                                                typeText = typeText ||  model.portfolio.assessmentType;
                                                element.text(typeText);
                                            });                                       
                        }
                    }
                };
    }
    /*return {
                type : 'directive',
                name : 'clientAssessmentTypeText',
                func : function(settingRepository)
                {
                     return {
                                restrict: 'A',
                                require: 'ngModel',
                                scope : {
                                            ngModel: '=?',
                                        },
                                link: function($scope, element, attrs,ngModel) 
                                {
                                    var model = $scope.ngModel;
                                    if(helper.isObject(model) && helper.isObject(model.portfolio) && helper.isString(model.portfolio.assessmentType))
                                    {
                                       var typeText = model.portfolio.assessmentType;
                                       settingRepository.getAsessmentTypes()
                                                        .then(function(types)
                                                        {
                                                            typeText = types[model.portfolio.assessmentType];
                                                            typeText = typeText ||  model.portfolio.assessmentType;
                                                            element.text(typeText);
                                                        });                                       
                                    }
                                }
                            };
                }
           };*/
});