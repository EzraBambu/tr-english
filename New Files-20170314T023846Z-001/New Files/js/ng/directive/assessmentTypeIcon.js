define(['helper'],function(helper)
{
    return function()
    {
         return {
                    restrict: 'A',
                    require: 'ngModel',
                    scope : {
                                ngModel: '=?',
                            },
                    link: function($scope, element, attrs,ngModel) 
                    {
                        var model = $scope.ngModel;
                        if(helper.isObject(model) && helper.isObject(model.portfolio) && helper.isString(model.portfolio.assessmentType))
                        {
                            var cssClass = 'profile-assessment-icon-indicator profile-' + model.portfolio.assessmentType;
                            element.addClass(cssClass);
                        }
                    }
                };
    };
    /*return {
                type : 'directive',
                name : 'assessmentTypeIcon',
                func : function()
                {
                     return {
                                restrict: 'A',
                                require: 'ngModel',
                                scope : {
                                            ngModel: '=?',
                                        },
                                link: function($scope, element, attrs,ngModel) 
                                {
                                    var model = $scope.ngModel;
                                    if(helper.isObject(model) && helper.isObject(model.portfolio) && helper.isString(model.portfolio.assessmentType))
                                    {
                                        var cssClass = 'profile-assessment-icon-indicator profile-' + model.portfolio.assessmentType;
                                        element.addClass(cssClass);
                                    }
                                }
                            };
                }
           };*/
});