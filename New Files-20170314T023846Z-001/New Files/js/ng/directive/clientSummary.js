define(['jquery','helper','text!view/shared/client/summary.html'],function($,helper,template)
{
    return  function($compile)
    {
         return {
                    restrict: 'E',
                    require: 'ngModel',
                    transclude: true,
                    replace: false,
                    template : template,
                    scope : {
                                ngModel: '=?',
                                ngClientSelect : '&'
                            },
                    link: function($scope, $element, attrs,ngModel) 
                    {
                        $scope.data = {
                                            client : $scope.ngModel
                                      };
                        $scope.handler = {};
                        $scope.handler.onClientClick = function(client)
                        {
                            if($scope.ngClientSelect)
                            {
                                var onClientSelect = $scope.ngClientSelect();
                                onClientSelect(client);
                            }
                        };
                    }
                };
    }        
});