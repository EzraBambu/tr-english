define(['helper'],function(helper)
{
    return function()
    {
         return {
                    restrict: 'A',
                    require: 'ngModel',
                    scope : {
                                ngModel: '=?',
                            },
                    link: function($scope, element, attrs,ngModel) 
                    {
                        var model = $scope.ngModel;
                        if(helper.isObject(model) && helper.isString(model.action) && model.action.length > 0)
                        {
                            var actionText = "";
                            var cssClass = '';
                            switch(model.action)
                            {
                                case 'buy':
                                    actionText = 'BUY';
                                    cssClass = 'positive-color action';
                                    break;
                                case 'sell':
                                    actionText = 'SELL';
                                    cssClass = 'negative-color action';
                                    break;
                                default :
                                    actionText = 'SELL';
                                    cssClass = 'positive-color action';
                                    break;
                            }
                            element.addClass(cssClass)
                                   .text(actionText);
                        }
                    }
                };
    }
    /*return {
                type : 'directive',
                name : 'reccomendationAction',
                func : function()
                {
                     return {
                                restrict: 'A',
                                require: 'ngModel',
                                scope : {
                                            ngModel: '=?',
                                        },
                                link: function($scope, element, attrs,ngModel) 
                                {
                                    var model = $scope.ngModel;
                                    if(helper.isObject(model) && helper.isString(model.action) && model.action.length > 0)
                                    {
                                        var actionText = "";
                                        var cssClass = '';
                                        switch(model.action)
                                        {
                                            case 'buy':
                                                actionText = 'BUY';
                                                cssClass = 'positive-color action';
                                                break;
                                            case 'sell':
                                                actionText = 'SELL';
                                                cssClass = 'negative-color action';
                                                break;
                                            default :
                                                actionText = 'SELL';
                                                cssClass = 'positive-color action';
                                                break;
                                        }
                                        element.addClass(cssClass)
                                               .text(actionText);
                                    }
                                }
                            };
                }
           };*/
});