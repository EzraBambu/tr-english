define(['app'],function(app)
{
    return function($rootScope)
    {
        var self = {};      

        self.broadcast = function(eventName,payload)
        {
            $rootScope.$broadcast(eventName,payload);
        };                   
       
        return self;
    }
    /*return {
                type : 'factory',
                name : 'eventService',
                func : function($rootScope)
                {
                    var self = {};      

                    self.broadcast = function(eventName,payload)
                    {
                        $rootScope.$broadcast(eventName,payload);
                    };                   
                   
                    return self;
                }
            };*/
});