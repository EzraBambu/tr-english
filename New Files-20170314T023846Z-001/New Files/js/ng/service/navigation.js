define(['app'],function(app)
{
    return function($rootScope,analyticsService)
    {
        var self = {};      
        var _routeByName = app._routeByName;                  

        $rootScope.$on(
                        '$stateChangeSuccess',
                        function(e,toState,toParams,fromState,fromParams)
                        {           
                            var route = _routeByName[toState.name];
                            analyticsService.logRoute(route);
                            $rootScope.$broadcast('select-menu',{name : route.metaData.selectedMenu,route : route});
                        }
                     );    

        return self;
    }
});