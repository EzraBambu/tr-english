/*
 * THIS SERVICE KNOW HOW TO COMMUNICATE TO HTTP BUT DON'T KNOW THE DATA
 */
define(['config','promise'],function(config,promise)
{
    return function($http)
    {
        var self = {};
        var baseUrl = config.endpoint.base;

        self.get = function(uri,payload)
        {
            return promise.defered()
                          .then(function(defered)
                          {
                                var url = baseUrl + '/' + uri + '?pv=' + Date.now();
                                $http(
                                        {
                                            method: 'GET', 
                                            url: url, 
                                            headers: {}                        
                                        }
                                    ) .then(function(response)
                                    {
                                        defered.resolve(response.data);
                                    })
                                    .catch(function(err)
                                     {
                                            defered.reject(err);
                                     });
                                return defered.promise;
                          });
        };
        return self;
    }
    /*return {
                func : function($http)
                {
                    var self = {};
                    var baseUrl = config.endpoint.base;

                    self.get = function(uri,payload)
                    {
                        return promise.defered()
                                      .then(function(defered)
                                      {
                                            var url = baseUrl + '/' + uri + '?pv=' + Date.now();
                                            $http(
                                                    {
                                                        method: 'GET', 
                                                        url: url, 
                                                        headers: {}                        
                                                    }
                                                ) .then(function(response)
                                                {
                                                    defered.resolve(response.data);
                                                })
                                                .catch(function(err)
                                                 {
                                                        defered.reject(err);
                                                 });
                                            return defered.promise;
                                      });
                    };
                    return self;
                }
           };*/
});