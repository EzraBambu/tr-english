define(['app','helper','promise','config','flow'],function(app,helper,promise,config,flow)
{
    return function($rootScope,$state)
    {
        var self = {};
        var _applicationByNameCache = {};
        var _routeLoadingCache = {};

        self.loads = function(list)
        {
            if(helper.isArray(list) === false)
            {
                list = [list];
            }
            return flow.each(
                                list,
                                function(routeFile)
                                {
                                    return self.load(routeFile);
                                }
                            );
        }

        self.load = function(criteria)
        {
            if(helper.isString(criteria))
            {
                criteria = { controller : criteria };
            }
            var cache = _routeLoadingCache[criteria.controller];
            if(cache === undefined)
            {
                return promise.defered()
                              .then(function(defered)
                              {
                                    _routeLoadingCache[criteria.controller] = defered.promise;                                                                                                                        
                                    promise.load('route/' + criteria.controller)
                                             .then(function(routes)
                                             {  
                                                    //ROUTE ENRICHMENT
                                                    routes = helper.map(
                                                                            routes,
                                                                            function(route)
                                                                            {
                                                                                route = helper.routeEnrichment(route);
                                                                                route.controller = route.controller || criteria.controller;
                                                                                return route;
                                                                            }
                                                                       );
                                                    helper.initRoute(app._globals.$stateProvider,routes);
                                                    defered.resolve();
                                             })
                                             .fail(function(err)
                                             {
                                                defered.reject(err);
                                             });
                                                                              
                                    //return defered.promise;
                                    return _routeLoadingCache[criteria.controller] ;
                              });
            }
            else
            {
                return cache;
            }
        }

        self.url = function(route,args)
        {            
            var appRoute;
            var url;
            args = args || {};
            if(helper.isString(route))
            {                            
                if(app._routeByName[route] !== undefined)
                {
                    appRoute = app._routeByName[route];
                }
            }
            else
            {
                //route.payload = route.payload || {};
                var routes = app._routes;
                var criteria = {controller : route.controller,action:route.action};
                if(criteria.action === undefined)
                {
                    criteria.action = 'index';
                }
                appRoute = helper.where(app._routes,criteria);
                if(appRoute.length > 0 )
                {
                    appRoute = appRoute[0];
                }                
            }
            if(appRoute && helper.isObject(appRoute.dependencies))
            {
                url = self.getApplicationBaseUrl() + appRoute.dependencies.url;
                if(route.payload)
                {
                    for(var key in route.payload)
                    {
                        url = url.replace('{' + key + '}',route.payload[key]);
                    }
                }
            }
            return url;
        };

        self.go = function(route,args)
        {       
            var appRoute;
            args = args || {};
            if(helper.isString(route))
            {                            
                if(app._routeByName[route] !== undefined)
                {
                    appRoute = app._routeByName[route];
                }
            }
            else
            {
                var routes = app._routes;
                var criteria = {controller : route.controller,action:route.action};
                if(criteria.action === undefined)
                {
                    criteria.action = 'index';
                }
                appRoute = helper.where(app._routes,criteria);
                if(appRoute.length === 0 )
                {
                    appRoute = {};
                }
                else
                {
                    appRoute = appRoute[0];
                }
            }
            $state.go(appRoute.name,args);
        };

        self.goToLandingPage = function()
        {
            return self.load(config.ng.defaultRoute)
                       .then(function()
                       {
                            self.go(config.ng.defaultRoute);
                       });
        }

        self.getApplicationByName = function(name)
        {
            name = name || 'app';
            var application = _applicationByNameCache[name];
            if(application === undefined)
            {
                application = helper.whereOne(app._routes,{name : name});
                _applicationByNameCache[name] = application
            }
            return application;
        };

        self.getApplicationBaseUrl = function(name)
        {
            var url = '';
            var application = self.getApplicationByName(name);
            if(helper.isObject(application) && helper.isObject(application.dependencies))
            {
                url = application.dependencies.url;
            }
            return url;
        }

        self.getMatchRouteByUrl = function(url,routes)
        {
            routes = routes || app._routes;
            var parts = url.split('/');
            var searchPart = parts.length;
            var appBaseUrl = self.getApplicationBaseUrl();
            var routeMatch;            
            helper.each(
                            routes,
                            function(route)
                            {
                                var routeUrl = appBaseUrl + route.dependencies.url;                                
                                var routeParts = routeUrl.split('/');
                                if(searchPart === routeParts.length)
                                {
                                    var payload = {};
                                    helper.each(
                                                    route.parameters,
                                                    function(parameterName)
                                                    {
                                                        var index = routeParts.indexOf('{' + parameterName + '}');
                                                        var parameterValue = parts[index];
                                                        if(parameterValue !== undefined)
                                                        {
                                                            routeParts[index] = parameterValue;
                                                            payload[parameterName] = parameterValue;
                                                        }
                                                    }
                                               );
                                    if(url === routeParts.join('/'))
                                    {
                                        routeMatch = {
                                                            route : route,
                                                            payload : payload
                                                     };
                                    }
                                }
                            }
                       );
            return routeMatch;
        }

        self.findRoute = function(criteria)
        {            
             return promise.pass(helper.where(app._routes,criteria));
        };

        self.isRouteLoaded = function(criteria)
        {
            /*return  self.findRoute(criteria)
                        .then(function(list)
                        {
                            return list.length > 0;
                        });*/
            return promise.pass(_routeLoadingCache[criteria.controller]  !== undefined);
        }

       
        return self;
    }
    /*return {
                func : function($rootScope,$state)
                {
                    var self = {};      

                    self.go = function(route,args)
                    {       
                        var appRoute;
                        args = args || {};
                        if(helper.isString(route))
                        {                            
                            if(app._routeByName[route] !== undefined)
                            {
                                appRoute = app._routeByName[route];
                            }
                        }
                        else
                        {
                            var routes = app._routes;
                            var criteria = {controller : route.controller,action:route.action};
                            if(criteria.action === undefined)
                            {
                                criteria.action = 'index';
                            }
                            appRoute = helper.where(app._routes,criteria);
                            if(appRoute.length === 0 )
                            {
                                appRoute = {};
                            }
                            else
                            {
                                appRoute = appRoute[0];
                            }
                        }
                        $state.go(appRoute.name,args);
                    };

                   
                    return self;
                }
            };*/
});