define(['helper','promise'],function(helper,promise)
{
    return function(BaseRepository)
    {
        function Repository()
        {
            BaseRepository.apply(this,Array.prototype.slice.call(arguments));
        }   
        helper.inherits(Repository,BaseRepository);

        Repository.prototype.getAsessmentTypes = function()
        {
            var self = this;
            return self.find({type : 'assessment-type'})
                       .then(function(list)
                       {
                            var map = {};
                            helper.each(
                                            list,
                                            function(item)
                                            {
                                                map[item.name] = item.description;
                                            }
                                       );
                            return map;
                       });
        }

        Repository.prototype.getByScreen = function(screenName,language)
        {
            var self = this;
            language = language || 'en';
            return self.find({type : 'label',screen : screenName})
                       .then(function(list)
                       {
                            return helper.each(list,function(item)
                            {
                                var value = item.value;
                                if(helper.isObject(value))
                                {
                                    item.value = item.value[language];
                                }
                                return item;
                            });
                       })
                       .then(function(list)
                       {
                            var map = {};
                            helper.each(
                                                list,
                                                function(item)
                                                {
                                                    map[item.name] = item.value;
                                                }
                                             );
                            return map;
                       });
        }

        Repository.prototype.getGlobalLabels = function()
        {
            var self = this;
            return self.getByScreen('global');
        }

        Repository.prototype.getClientListLabels = function()
        {
            var self = this;
            return self.getByScreen('client-list');
        }

        var self = new Repository('setting');
        return self;
    }


    /*return {
                type : 'factory',                
                func : function(BaseRepository)
                {
                    function Repository()
                    {
                        BaseRepository.apply(this,Array.prototype.slice.call(arguments));
                    }   
                    helper.inherits(Repository,BaseRepository);

                    Repository.prototype.getAsessmentTypes = function()
                    {
                        var self = this;
                        return self.find({type : 'assessment-type'})
                                   .then(function(list)
                                   {
                                        var map = {};
                                        helper.each(
                                                        list,
                                                        function(item)
                                                        {
                                                            map[item.name] = item.description;
                                                        }
                                                   );
                                        return map;
                                   });
                    }

                    Repository.prototype.getByScreen = function(screenName,language)
                    {
                        var self = this;
                        language = language || 'en';
                        return self.find({type : 'label',screen : screenName})
                                   .then(function(list)
                                   {
                                        return helper.each(list,function(item)
                                        {
                                            var value = item.value;
                                            if(helper.isObject(value))
                                            {
                                                item.value = item.value[language];
                                            }
                                            return item;
                                        });
                                   })
                                   .then(function(list)
                                   {
                                        var map = {};
                                        helper.each(
                                                            list,
                                                            function(item)
                                                            {
                                                                map[item.name] = item.value;
                                                            }
                                                         );
                                        return map;
                                   });
                    }

                    Repository.prototype.getGlobalLabels = function()
                    {
                        var self = this;
                        return self.getByScreen('global');
                    }

                    Repository.prototype.getClientListLabels = function()
                    {
                        var self = this;
                        return self.getByScreen('client-list');
                    }

                    var self = new Repository('setting');
                    return self;
                }
           };*/
});