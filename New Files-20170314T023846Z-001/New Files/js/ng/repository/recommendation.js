define(['helper','flow','promise'],function(helper,flow,promise)
{
    return function(BaseRepository)
    {
        var _clientRepository;

        function Repository()
        {
            BaseRepository.apply(this,Array.prototype.slice.call(arguments));
        }
        helper.inherits(Repository,BaseRepository);

        Repository.prototype.getMarketInsight = function()
        {
            return self.find()
                       .then(function(recommendations)
                       {
                            return flow.each(
                                                recommendations,
                                                function(recommendation)
                                                {
                                                    var impact = {positive:0,negative:0};
                                                    if(helper.isArray(recommendation.client))
                                                    {
                                                        helper.each(
                                                                        recommendation.client,
                                                                        function(client)
                                                                        {
                                                                            if(impact[client.effect] !== undefined)
                                                                            {
                                                                                impact[client.effect] += 1;
                                                                            }
                                                                        }
                                                                   );
                                                    }
                                                    recommendation.impact = impact;
                                                    return recommendation;
                                                }
                                            )
                                        .then(function()
                                        {
                                            return recommendations;
                                        });
                       });
        }

        Repository.prototype.getClients = function(recommendation)
        {
            var clientIds = helper.pluck(recommendation.client,'id');            
            return _getClientRepository().then(function(clientRepository)
                                        {
                                            return clientRepository.find({id : {$in : clientIds }})
                                                                   .then(function(clients)
                                                                   {
                                                                        return clients;
                                                                   });
                                        });
        }




        Repository.prototype.getByClientId = function(clientId)
        {
            var self = this;                        
            return self.find({'client.id':clientId});
        }

        Repository.prototype.getClientImpact = function(clientId)
        {
            return self.getByClientId(clientId)
                       .then(function(recommendations)
                       {
                            var impact = {positive:0,negative:0};
                            return flow.each(
                                                recommendations,
                                                function(recommendation)
                                                {
                                                    if(helper.isArray(recommendation.client))
                                                    {
                                                        helper.each(
                                                                        helper.where(recommendation.client,{id : clientId}),
                                                                        function(record)
                                                                        {
                                                                            if(impact[record.effect] !== undefined)
                                                                            {
                                                                                impact[record.effect] += 1;
                                                                            }
                                                                        }
                                                                   );
                                                    }
                                                    return impact;
                                                }
                                            )
                                        .then(function()
                                        {
                                            return impact;
                                        });
                       });
        }


        function _getClientRepository()
        {
            /* MAKE A LATE BINDING TO AVOID CYCLE RELATIONSHIP SINCE RIGHT NOW CLIENT REQUIRE RECOMMENDATION */
            if(_clientRepository === undefined)
            {
                  return helper.loadDependencies(['clientRepository'])
                             .then(function()
                             {
                                    _clientRepository = helper.getService('clientRepository');
                                    return promise.pass(_clientRepository);
                             });
             }
             else
             {
                return promise.pass(_clientRepository);
             }
        }

        var self = new Repository('reccomendation');
        return self;
    }
    
    /*return {
                type : 'factory',                
                func : function(BaseRepository)
                {
                    function Repository()
                    {
                        BaseRepository.apply(this,Array.prototype.slice.call(arguments));
                    }
                    helper.inherits(Repository,BaseRepository);

                    Repository.prototype.getMarketInsight = function()
                    {
                        return self.find()
                                   .then(function(recommendations)
                                   {
                                        return flow.each(
                                                            recommendations,
                                                            function(recommendation)
                                                            {
                                                                var impact = {positive:0,negative:0};
                                                                if(helper.isArray(recommendation.client))
                                                                {
                                                                    helper.each(
                                                                                    recommendation.client,
                                                                                    function(client)
                                                                                    {
                                                                                        if(impact[client.effect] !== undefined)
                                                                                        {
                                                                                            impact[client.effect] += 1;
                                                                                        }
                                                                                    }
                                                                               );
                                                                }
                                                                recommendation.impact = impact;
                                                                return recommendation;
                                                            }
                                                        )
                                                    .then(function()
                                                    {
                                                        return recommendations;
                                                    });
                                   });
                    }


                    Repository.prototype.getByClientId = function(clientId)
                    {
                        var self = this;                        
                        return self.find({'client.id':clientId});
                    }

                    Repository.prototype.getClientImpact = function(clientId)
                    {
                        return self.getByClientId(clientId)
                                   .then(function(recommendations)
                                   {
                                        var impact = {positive:0,negative:0};
                                        return flow.each(
                                                            recommendations,
                                                            function(recommendation)
                                                            {
                                                                if(helper.isArray(recommendation.client))
                                                                {
                                                                    helper.each(
                                                                                    helper.where(recommendation.client,{id : clientId}),
                                                                                    function(record)
                                                                                    {
                                                                                        if(impact[record.effect] !== undefined)
                                                                                        {
                                                                                            impact[record.effect] += 1;
                                                                                        }
                                                                                    }
                                                                               );
                                                                }
                                                                return impact;
                                                            }
                                                        )
                                                    .then(function()
                                                    {
                                                        return impact;
                                                    });
                                   });
                    }

                    var self = new Repository('reccomendation');
                    return self;
                }
           };*/
});