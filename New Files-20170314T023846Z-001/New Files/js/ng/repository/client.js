define(['helper','flow'],function(helper,flow)
{
    return function(BaseRepository,recommendationRepository)
    {
        function Repository(tableName)
        {
            BaseRepository.apply(this,Array.prototype.slice.call(arguments));
        }
        helper.inherits(Repository,BaseRepository);
        var self = new Repository('client');

        self.find = function(criteria)
        {
            return self.base                                    
                       .find.apply(self,[criteria])
                       .then(function(clients)
                       {                                        
                            return flow.each(
                                                clients,
                                                function(client)
                                                {     
                                                    return recommendationRepository.getClientImpact(client.id)
                                                                                   .then(function(impact)
                                                                                   {
                                                                                        client.impact = impact;
                                                                                        return client;
                                                                                   });                                                                
                                                }
                                            );

                       });
        };

        self.getRecommendations = function(clientId)
        {
            return flow.waterfall(
                                    {
                                        client : function()
                                        {
                                            return self.findById(clientId);
                                        },
                                        recommendations : function()
                                        {
                                            return recommendationRepository.getByClientId(clientId);
                                        }
                                    }
                                 )
                        .then(function(response)
                        {     
                            var client = response.client;
                            var recommendations = response.recommendations;
                            var orderRecommendations;
                            if(client.recommendationOrder === undefined || client.recommendationOrder.length == 0)
                            {
                                orderRecommendations = recommendations;
                            }
                            else
                            {                                            
                                orderRecommendations = [];
                                helper.each(
                                                client.recommendationOrder,
                                                function(recommendationId)
                                                {
                                                    var results = helper.where(recommendations,{id : recommendationId});
                                                    if(results.length > 0)
                                                    {
                                                        orderRecommendations.push(results[0]);
                                                    }
                                                }
                                           );
                                if(orderRecommendations.length !== recommendations.length)
                                {
                                    orderRecommendations = orderRecommendations.concat(
                                                                                          helper.filter(  
                                                                                                          recommendations,
                                                                                                          function(recommendation)
                                                                                                          {
                                                                                                              return client.recommendationOrder.indexOf(recommendation.id) === -1;
                                                                                                          }
                                                                                                       )
                                                                                       );
                                }
                            }

                            return orderRecommendations;
                        });
        };

        return self;
    };

    /*return {
                type : 'factory',              
                func : function(BaseRepository,recommendationRepository)
                {
                    function Repository(tableName)
                    {
                        BaseRepository.apply(this,Array.prototype.slice.call(arguments));
                    }
                    helper.inherits(Repository,BaseRepository);
                    var self = new Repository('client');

                    self.find = function(criteria)
                    {
                        return self.base                                    
                                   .find.apply(self,[criteria])
                                   .then(function(clients)
                                   {                                        
                                        return flow.each(
                                                            clients,
                                                            function(client)
                                                            {     
                                                                return recommendationRepository.getClientImpact(client.id)
                                                                                               .then(function(impact)
                                                                                               {
                                                                                                    client.impact = impact;
                                                                                                    return client;
                                                                                               });                                                                
                                                            }
                                                        );

                                   });
                    };

                    self.getRecommendations = function(clientId)
                    {
                        return flow.waterfall(
                                                {
                                                    client : function()
                                                    {
                                                        return self.findById(clientId);
                                                    },
                                                    recommendations : function()
                                                    {
                                                        return recommendationRepository.getByClientId(clientId);
                                                    }
                                                }
                                             )
                                    .then(function(response)
                                    {     
                                        var client = response.client;
                                        var recommendations = response.recommendations;
                                        var orderRecommendations;
                                        if(client.recommendationOrder === undefined || client.recommendationOrder.length == 0)
                                        {
                                            orderRecommendations = recommendations;
                                        }
                                        else
                                        {                                            
                                            orderRecommendations = [];
                                            helper.each(
                                                            client.recommendationOrder,
                                                            function(recommendationId)
                                                            {
                                                                var results = helper.where(recommendations,{id : recommendationId});
                                                                if(results.length > 0)
                                                                {
                                                                    orderRecommendations.push(results[0]);
                                                                }
                                                            }
                                                       );
                                            if(orderRecommendations.length !== recommendations.length)
                                            {
                                                orderRecommendations = orderRecommendations.concat(
                                                                                                      helper.filter(  
                                                                                                                      recommendations,
                                                                                                                      function(recommendation)
                                                                                                                      {
                                                                                                                          return client.recommendationOrder.indexOf(recommendation.id) === -1;
                                                                                                                      }
                                                                                                                   )
                                                                                                   );
                                            }
                                        }

                                        return orderRecommendations;
                                    });
                    };

                    return self;
                }
           };*/
});