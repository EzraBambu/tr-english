define(['helper','flow'],function(helper,flow)
{
    var cssMapByEffect = {
                            positive : 'positive-color',
                            negative : 'negative-color',
                            neutral :  'neutral-color',
                         };
                         
    return function($scope,$timeout,newsRepository,client,reccomendations)
    {        
        var $recommendationList;            
        $scope.data = {
                            client : client,
                            reccomendations : reccomendations,
                            selectedRecommendation : {},
                            news : []
                      };
        if(reccomendations.length > 0)
        {
            $timeout(function()
            {
                 $recommendationList = $('.recommendation-list');
                _setSelectedRecommendation(reccomendations[0]);
            },500);
        }
        $scope.handler = {};
        $scope.handler.onClickRecommendation = function(recommendation,index)
        {
            _setSelectedRecommendation(recommendation);
            return false;
        };

        $scope.handler.getIcon = function(data)
        {
            var src = '';                        

            if(helper.isObject(data))
            {
                if(data.icon === 'star')
                {
                    src = 'star.png';
                }
                else
                {
                    //CIRCLE
                    src = 'circle.png';
                }
                if(src.length > 0 && helper.isString(data.effect) && data.effect.length > 0)
                {   
                    src = 'img/' + data.effect + '-' + src;
                }
                else
                {
                    console.log(data);
                }
            }
            return src;
        }

        $scope.handler.onClickCategoryDone = function()
        {
            var recommendation = $scope.data.selectedRecommendation;
            if(recommendation && reccomendations.length > 0)
            {
                var index = reccomendations.indexOf(recommendation);
                if(index >= 0)
                {
                    reccomendations.splice(index,1);
                    if(reccomendations.length > 0)
                    {
                        _setSelectedRecommendation(reccomendations[0]);
                    }
                    else
                    {
                        $scope.data.news = [];
                    }
                }
            }
        }

        function _setSelectedRecommendation(recommendation)
        {
            $scope.data.selectedRecommendation = recommendation;

            //console.log($('.recommendation',$recommendationList));

            $('.recommendation > div',$recommendationList).removeClass('selected');
            $('#recommendation-'+ recommendation.id).addClass('selected');
            if(helper.isArray(recommendation.newsList))
            {
                newsRepository.getNews(recommendation.newsList)
                              .then(function(list)
                              {
                                    $scope.data.news = list;
                                    $scope.$digest();                                                
                              });
            }
        }
    }
});