define(['helper'],function(helper)
{
    return function($scope,recommendations)
    {
        var self = this;
        $scope.data = {
                            recommendations : recommendations
                      };
        $scope.handler = {};
        
        $scope.handler.getIcon = function(data)
        {
            var src = '';
            if(helper.isObject(data))
            {
                if(data.icon === 'star')
                {
                    src = 'star.png';
                }
                else
                {
                    //CIRCLE
                    src = 'circle.png';
                }
                if(src.length > 0 && helper.isString(data.effect) && data.effect.length > 0)
                {   
                    src = 'img/' + data.effect + '-' + src;
                }
                else
                {
                    console.log(data);
                }
            }
            return src;
        };

        $scope.handler.onRecommendationClick = function(recommendation)
        {
            self.gotoAction('detail',{recommendationId : recommendation.id});
        }
    }       
});