require([
            'app',            
            'domReady',            
            'config',
            'angularAMD',
            'angularSlider',
            'bootstrap'
        ],function(app,domReady,config  /*jQuery,angularAMD,util,func,routeResolver,config,axios,q*/)
{    
    domReady(function() 
    {            
        //LOAD THE ROUTE FILE NOW SO THAT LATER WE CAN JUST REFER TO IT
        require(
                    config.ng.route,
                    function()
                    {
                        angular.bootstrap(document, [config.app.name]);
                    }
                );
    });
});